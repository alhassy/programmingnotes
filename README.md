#Summary#

These notes document my adventures when programming with C\#, Haskell,
and their interactions with Category Theory.

### How do I get set up? ###

A genereated PDF is in the repo.

The setup I am using is the following:

* Emacs, https://www.gnu.org/software/emacs/
* AucTex for using LaTeX on Emacs, https://www.gnu.org/software/auctex/
* Aspell for basic spell-checking, http://emacswiki.org/emacs/AspellWindows
* An old version of folding-mode, http://www.emacswiki.org/emacs/FoldingMode

### Contribution guidelines ###

Any comments and/or feedback are most welcome!