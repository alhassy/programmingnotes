open import Level
module auxiliary where

lsp : ∀{T S : Set}{Q : T → Set} →
  (∀ {x : T} → Q x) → ( _′ : S → T ) → (∀ {x : S} → Q (x ′))
lsp QT _′ = λ{s} → QT {s ′}
