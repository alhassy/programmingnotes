\documentclass{article}

%{{{ Header

%{{{ packages
\usepackage[hmargin=20mm,top=12mm,bottom=20mm]{geometry}
\usepackage{xcolor}
\usepackage{CalcStyleV7}
\usepackage{amsmath}

\usepackage{hyperref}
%}}}

\begin{document}

%{{{ firstpage content
\centerline{\sc \large Programming Notes}
\vspace{.5pc}
\centerline{\sc Musa Al-hassy}
\vspace{.5pc}
\centerline{\today}
\vspace{2pc}

%% READ ME : generate pdf by doing :
%%
%% pdflatex programmingnotes.ltx

These notes document my adventures when programming with C\#, Haskell,
and their interactions with Category Theory.

\newcommand{\ignore}[1]{}

\begin{quote} \it
  * Take one step towards the goal, then worry about the next step once you've
  made the first.

  * \href{http://en.wikipedia.org/wiki/Agile_software_development#Philosophy}%
  {Not trying to solve the whole problem, just take one step, then look where to take
    the next step.}
\end{quote}

\tableofcontents

%}}}
%}}}

%{{{ Polymorphism
\section{Polymorphism}

The purpose of polymorphism is to support code abstraction, thus permitting re-usability. 
The three most common forms are subtype (OOP), parametric (functional),
and ad-hoc (overloading) polymorphism.

\subsection{Subtype Polymorphism}
\label{subtype:intro}

Suppose we have a function \[f : X_0 \to Y\] then
we cannot make the call $f(x_0, x_1, \cdots, x_n)$ as it does not type-check,
which is silly since we're giving the function $f$ more information by passing
it more than what it needs.
Instead we need to define a family of functions, for any $n$, 
\[ X_0 \times X_1 \times \cdots \times X_n \to Y : (x_0, x_1, \cdots, x_n) \mapsto f \ x_0\]
\emph{The programmer is forced to write the same thing multiple times!}

This is rectified by introducing subtyping, which is characterized by
\[
  \text{subsumption rule} ::
  \text{ if } e : A \text{ and } A \leq B \text{ then } e : B
\]
That is, whenever we can show $A$ is a subtype of $B$, then we can treat an $A$ expression
as if it were a $B$ expression. The other axioms introduced for the subtype relation are reflexivity and
transitivity, thus making it a preorder.
\\
({\bf Aside:} Subtyping can be mimicked in a type theory that doesn't have it by defining $A \leq B$
to be a surjection of $A$ onto $B$. Then the subsumption rule would be syntactic sugar for applying this
canonical surjection.
)

Then we introduce an axiom for products,
\[
  \text{product subtyping :: }
  \text{ if } m \geq n \text{ and } (\forall i : 0 .. n \spot A_i \leq B_i)
  \text{ then }
  A_0 \times A_1 \times \cdots A_m \leq   A_0 \times A_1 \times \cdots \times A_n
\]
That is, a (larger) $m$ tuple can be used in lieu of a (smaller) $n$ tuple.
Intuitively this makes sense, since we just `forget' \footnote{ToDo: subtyping as a forgetful functor?}
the extra elements, and for each component of the tuple we forget any additional structure.

The case $A_i = B_i$ is called \emph{width subtyping}; the case
$m = n$ is called \emph{deep subtyping} and it's just saying
\emph{the subtype relation is multiplicatively monotonic}.

Needless to say, tuples when named are usually called \emph{records}, or classes.

\subsection{Top and bottom}
All types are bounded below by the empty type and above by the most generic type,
which is \texttt{object} in C\#.

\subsection{``Be liberal in what you accept, and conservative in what you send''}
\label{subtype:function}
This phrase not only applies to protocol implementation but to life as well;
take a moment to seriously reflect on this.

The notion of accept-send is abstracted as a function. Then this wisdom is
tantamount to saying,
\[ \text{if } A \leq B  \text{ and } C \leq D \text{ then }
  (B \to C) \leq (A \to D)
\]
The last bit is just the quote but reading the last $\leq$ from right-to-left:
We accepted $A$ before, but now are more liberal and accept $B$;
We sent out $D$ before, but now are more conservative and only send out $C$.

That is,
one can constrain their domain of operation by accepting more specific information.
Likewise, one can expand their range of output by outputting the usual but forgetting
some information.

A mnemonic to remember this is that the arrow between the innards of the formula
$A \leq B \,\land\, C \leq D$ can be stretched into an arrow between the ends
of the formula.

({\bf Aside:} 
\href{https://www.fpcomplete.com/school/to-infinity-and-beyond/pick-of-the-week/profunctors}{In the Haskell community, one may find that this is a `profunctor'.}
)
%}}}

%{{{ Variance
\section{Variance}

\subsection{Introduction}
The notion of variance is primarily for OOP-compatible languages, and
its usage increases the number of well-typed programs.
Haskell, for example, has no notion of 'sub-type' and as such this topic
does not apply to it ---however it makes usage of the terms in their more natural
setting of category theory, but that's not of import for now.

It can be argued that variance is the trouble one pays for admitting a notion of
subtyping.

\subsection{From Category Theory}
A poset may be construed as a category whose objects are the elements and whose
arrows are pairs $(a,b)$ such that $a \leq b$, with source
and target being the usual pair projections. Monotone and antitone functions are
then special cases of \emph{covariant and contravaraint} functors between the 
associated poset categories.

({\bf Aside:}
In colloquial English, the prefix `co' means joint or mutual, whereas
the prefix `contra' means against or opposite.
)

Types are ordered by the sub-type relation, which is characterized by the 
\[
  \text{subsumption rule} ::
  \text{ if } e : A \text{ and } A \leq B \text{ then } e : B
\]
We may read $A \leq B$ as \emph{it is possible to use $A$ where $B$ is expected.}
The simplest example of this is when $A$ is a class derived from $B$ase class $B$.
Perhaps even more simply, we have, in C\# for example, that $A \leq \texttt{Object}$
---ie it is terminal, that is, it is the top of our types.

Then a covariant type-former is just a monotonic function over this ordered space.
That is, if $F$ is a type former, such as \texttt{IEnumerable, List, Maybe, Nullable},
then
\begin{itemize}
\item[] $F$ is \emph{covariant} ---it preserves the sub-type relation
\item[=] $A \leq B \Rightarrow F(A) \leq F(B)$
\item[=] when an $F(B)$ element is requested
we may supply an $F(A)$ element instead, provided $A$ is a sub-type of $B$
\item[=] one can use $F(A)$ in places where $F(B)$ is expected, provided $A \leq B$.
\end{itemize}
In C\# we inform the compiler that $F$ is covaraint in a type parameter by prefixing
the parameter with the \texttt{out} keyword.

Dually,
\begin{itemize}
\item[] $F$ is \emph{contravariant} ---it reverses the sub-type relation
\item[=] $A \leq B \Rightarrow F(A) \geq F(B)$
\item[=] when an $F(A)$ element is requested
we may supply an $F(B)$ element instead, provided $A$ is a sub-type of $B$
\item[=] one can use $F(B)$ in places where $F(A)$ is expected, provided $A \leq B$.
\end{itemize}
In C\# we inform the compiler that $F$ is contravaraint in a type parameter by prefixing
the parameter with the \texttt{in} keyword.

Some common examples:
\begin{enumerate}
\item products are covaraint, see \autoref{subtype:intro}
\item ``be liberal in what you accept, and conservative in what you send''
---this is nothing more  than function space variance! See \autoref{subtype:function}.
\end{enumerate}

\subsubsection*{Mixing Functors}
By following the definitions given above, it can be easily seen that:
\begin{center}
If $F$ and $G$ have the same variance then their compositions are both covaraint,
otherwise their compositions are both contravaraint.
\end{center}

For example, C\#'s \texttt{Action<T>} and \texttt{IComparer<T>} are both contravaraint,
while \texttt{IEnumerable<T>} is covaraint. Hence,
\texttt{Action<IComparer<T>>} is covaraint but \texttt{Action<IEnumerable<T>>} is not.

\subsection{Back to C\#}
Of course in programming there is no need to enforce the coherence laws of functors
and indeed type-formers may not be functors at all, and then we say they are
\emph{invariant}. That is, they ignore the subtype relation all-together.

In C\# this is the default; i.e., by not using the \texttt{in, out} annotations.
\texttt{IList} for example is invariant.

At the other end of the spectrum are \emph{bivariant} type-formers that are both
covariant and contravariant at the same time:
$A \leq B \Rightarrow F(A) \leq F(B) \leq F(A)$.
%%
A C\# example of this would be a an empty interface: 
\texttt{interface I<A> \{ \} }.

Read-only datatypes are covariant, write-only are contravariant, and mutable
datatypes are invaraint. \marginpar{Why?}

This leads me to ask whether coalgebras and algebras are covaraint and contravaraint?
Something to think about, for future-musa.

\subsection{Contravariance as a principle}

The so-called \emph{Liskov Substitution Principle} states, as far as I know,
\begin{center}
provable properties of a type are also provable of all its subtypes
\end{center}
That is, letting $Q(X) := (\forall x : T \spot q \ x)$,
\[ S \leq T \;\implies\; \left(\, Q(T) \;\implies\; Q(S) \, \right) \]
Indeed, to prove $Q(S)$ we simply `forget' the extra structure they contain
and construe them as element of $T$, then we are proving $Q(T)$, which is true
by assumption.

What this says, if we interpret proofs as types a la Curry-Howard, then is
that $Q$ is contravaraint.

(
This principle is a part of the \href{http://hanselminutes.com/145/solid-principles-with-uncle-bob-robert-c-martin}{SOLID} software development principles;
which are expressed humorously \href{https://lostechies.com/derickbailey/2009/02/11/solid-development-principles-in-motivational-pictures/}{here}.
)

\subsection{Works consulted}

\begin{itemize}
\item \url{http://tomasp.net/blog/variance-explained.aspx}

Explains what we've covered but assuming only familiarity with C\#; very accessible.

\item \url{https://apocalisp.wordpress.com/2010/10/06/liskov-substitution-principle-is-contravariance/}

Explains that LSP is contravaraince.

\subsection{Further Reading ---stuff I intend to read}

\item \url{http://www.stephanboyer.com/post/39/covariance-and-contravariance}

\item eric lippert \url{http://blogs.msdn.com/b/ericlippert/archive/2007/10/16/covariance-and-contravariance-in-c-part-one.aspx}

\item \url{https://msdn.microsoft.com/en-us/magazine/ff796223.aspx}

Chris Burrows on varaince

\item \url{https://channel9.msdn.com/shows/Going+Deep/E2E-Brian-Beckman-and-Erik-Meijer-CoContravariance-in-Physics-and-Programming-2-of-2/}

Meijer and Beckman on varaince, programming, and physics

\end{itemize}
%}}}

\ignore{
%{{{ Reading List
\section{Reading List}
\begin{itemize}

\item Foundations of Object-Oriented Programming Languages by Kim B. Bruce

\item lippert \url{http://blogs.msdn.com/b/ericlippert/archive/2011/02/03/curiouser-and-curiouser.aspx}

\item \url{http://blogs.msdn.com/b/shawnhar/archive/2011/04/05/visitor-and-multiple-dispatch-via-c-dynamic.aspx}

\item \url{https://en.wikipedia.org/wiki/Type_system}

\item \url{http://geekswithblogs.net/abhijeetp/archive/2009/01/10/dynamic-attributes-in-c.aspx}

\item \url{http://www.codefugue.com/haskell-in-c-sharp-functors/}

\item \url{https://en.wikipedia.org/wiki/Subtyping}

\item \url{https://en.wikipedia.org/wiki/Covariance_and_contravariance_(computer_science)}

\item programming with continuations \url{http://www.cs.indiana.edu/pub/techreports/TR151.pdf}

\item the discoveries of continuations \url{http://www.cs.cmu.edu/afs/cs/user/jcr/ftp/histcont.pdf}

\item a short intro to call/cc \url{http://community.schemewiki.org/?call-with-current-continuation}

\url{https://en.wikipedia.org/wiki/Call-with-current-continuation}

\item study of cps transform
\url{http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=446CB8FCBDDF9B9AEB4F962F01B92C9C?doi=10.1.1.46.84&rep=rep1&type=pdf}

\item \url{https://en.wikipedia.org/wiki/Continuation}

\item \url{http://okmij.org/ftp/continuations/index.html}

\item \url{http://www.csse.monash.edu.au/~lloyd/tilde/Semantics/}

\item \url{http://www.madore.org/~david/computers/callcc.html}

\item \url{http://blog.sigfpe.com/2006/12/yonedic-addendum.html}

\url{http://blog.sigfpe.com/2006/11/yoneda-lemma.html}

\item \url{https://reperiendi.wordpress.com/2007/11/03/category-theory-for-the-java-programmer/}

\item curse of LEM \url{http://queue.acm.org/detail.cfm?ref=rss&id=2611829}

\item \url{http://blogs.msdn.com/b/csharpfaq/archive/2012/06/26/understanding-a-simple-async-program.aspx}

\end{itemize}
%}}}
}

%{{{ Quotes
\part*{Quotes}
A collection of quotes that I like :-)

From \href{https://en.wikiquote.org/wiki/C._A._R._Hoare}{C.A.R. Hoare},
\begin{itemize}
\item There are two ways of constructing a software design: 
One way is to make it so simple that there are obviously no deficiencies, 
and the other way is to make it so complicated that there are no obvious
deficiencies. The first method is far more difficult. 

\item The price of reliability is the pursuit of the utmost simplicity.
It is a price which the very rich find most hard to pay.

\item The real value of tests is not that they detect bugs in the code,
but that they detect inadequacies in the methods, concentration, and
skills of those who design and produce the code.

\item Premature optimization is the root of all evil.
\end{itemize}

From \href{https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar}{\emph{The Cathedral and the Bazaar}},
\begin{itemize}
\item If you have the right attitude, interesting problems will find you.
\item To solve an interesting problem, start by finding a problem that is interesting to you.
\end{itemize}

From \href{http://pu.inf.uni-tuebingen.de/users/klaeren/epigrams.html}{\emph{Epigrams on Programming}}
\footnote{An epigram is a remark expressing an idea in a clever/amusing way.}
\begin{itemize}
\item If a program manipulates a large amount of data, it does so in a small number of ways.
\item Symmetry is a complexity reducing concept; seek it everywhere.
\item It is easier to write an incorrect program than understand a correct one.
\item A programming language is low level when its programs require attention to the irrelevant.

(Memory de/allocation is not central to my problem, why must I worry about it! I'm looking at you C!)

\item If a listener nods his head when you're explaining your program, wake him up.

\item Simplicity does not precede complexity, but follows it.

\item It is easier to change the specification to fit the program than vice versa.

\item Fools ignore complexity. Pragmatists suffer it. Some can avoid it. Geniuses remove it.

\item A year spent in artificial intelligence is enough to make one believe in God.

\item Dealing with failure is easy: Work hard to improve.
Success is also easy to handle: You've solved the wrong problem.
Work hard to improve.

\item You think you know when you learn, are more sure when you can write,
even more when you teach, but certain when you can program.

[ If you want to learn something, say graph theory, then program it ... in Agda ;) ]

\item It does against the grain of modern education to teach children to program.
What fun is there in making plans, acquiring discipline in organizing thoughts,
devoting attention to detail and learning to be self-critical.
\end{itemize}

%}}}

\end{document}
% Local Variables:
% folded-file: t
% eval: (fold-set-marks "%{{{ " "%}}}")
% eval: (fold-whole-buffer)
% fold-internal-margins: 0
% end:
